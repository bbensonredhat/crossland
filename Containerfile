# crossland is designed to be built from this Containerfile via the buildah bud invoked in the Makefile

#ARG

#ENV

#FROM quay.io/centos-sig-automotive/autosd-buildbox:latest
FROM quay.io/centos/centos:stream9
#FROM quay.io/centos/centos:stream9-minimal

LABEL com.github.containers.toolbox="true" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image can be used with the toolbox command" \
      summary="Base image for building AutoSD packages with cross compilation tools" \
      maintainer="Bruce Benson <bbenson@redhat.com>" \
      authors="Leonardo Rossetti, Stephen Smoogen, Lester Claudio, Bruce Benson"

#ENTRYPOINT 

#CMD

#### BASE OS PREP
RUN dnf update -y && \
    dnf install -y 'dnf-command(config-manager)' epel-release

# enable crb and autosd repositories
RUN dnf config-manager --set-enabled crb && \
    dnf config-manager --add-repo https://buildlogs.centos.org/9-stream/automotive/$(arch)/packages-main/ && \
    dnf config-manager --add-repo https://buildlogs.centos.org/9-stream/autosd/$(arch)/packages-main/

# copy configuration files to the filesystem
# TODO check rpms.in between the upstream native and cross 
# TODO check rpms.in for dupes with the groupinstall above
# TODO check rpms.in to be able to undefine the fixed versions, or have separate files 
COPY rpms.in /rpms.in
COPY rpms-cross-tools.in /rpms-cross-tools.in

# TODO check autosd-buildbox for missing items from rpms.txt

#### NATIVE

# Install additional desired general packages
RUN dnf -y --skip-broken --best --allowerasing install $(cat /rpms.in) 
RUN dnf update -y

# Install package utility tools 
RUN dnf install -y centpkg koji

#### BASE OS READY
# at this point, there is a basic system to navigate around in under whatever arch this container was built to

# Install capability to compile natively
# TODO: currently a bit of kitchen sink, convert to list of rpms
RUN dnf -y groupinstall "Development Tools"

#### CROSS
# Install the cross-arch tools packages
# disable epel-release to ensure we don't install the cross-compiler from EPEL instead
# TODO use rpms-cross-tools.in
RUN koji -p stream download-build -a x86_64 --latestfrom=c9s-gate gcc && \
    koji -p stream download-build -a x86_64 --latestfrom=c9s-gate binutils && \
    dnf install --disablerepo=epel -y ./cross-binutils-aarch64*.rpm \
        ./cross-gcc-aarch64*.rpm \
        ./cross-gcc-c++-aarch64*.rpm

# also get another cross-arch tools set for external packaging ('pack' target in Makefile)
RUN mkdir -p /opt/cross-tools
RUN dnf install --disablerepo=epel \
    --installroot=/opt/cross-tools \
    --nogpgcheck \
    --releasever=9 -y \
    ./cross-gcc-aarch64*.rpm \
    ./cross-binutils-aarch64*.rpm \
    ./cross-gcc-c++-aarch64*.rpm


# sysroot for includes and libraries
# TODO: install from a list of rpms
# TODO: layer the sysroot container although that was built on the target arch) and stop installing from here
RUN mkdir -pv /usr/aarch64-redhat-linux/sys-root/el9 && \
    dnf --forcearch=aarch64 \
    --installroot=/usr/aarch64-redhat-linux/sys-root/el9 \
    --nogpgcheck \
    --releasever=9 install -y \
    glibc-devel libstdc++-devel libzstd-devel \
    binutils-devel e2fsprogs-devel fuse-devel \
    gcc-plugin-devel glib2-devel libcap-devel \
    libcap-ng-devel libmnl-devel numactl-devel \
    openssl-devel perl-devel python3-devel gpgme-devel

# buildroot
#TODO dnf install ncurses ncurses-devel #for make menuconfig

# cleanup
RUN dnf clean all

#ONBUILD
