# Crossland
## AutoSD Cross Compilation Tooling and Environment

This container image provides a development environment for those who want to
perform cross compilation tasks for/on their AutoSD build stack.

Currently works for:
| build <br>(where compiler is built) | host <br>(where compiler does building) | target<br> (triplet) | freestanding | static | dynamic | sysroot |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| x86-64 | x86-64 | x86_64-redhat-linux |  gcc  |   | gcc  |   |
| x86_64    | x86-64 | aarch64-redhat-lnux |  gcc  |   	 |  gcc   |      |

For many people, this can be understood as something like  "I use this to run a compiler in a container on x86-64 that makes binaries targeted for aarch64"

(c.f. [GCC Terms](https://gcc.gnu.org/onlinedocs/gccint/Configure-Terms.html) and [GCC Archs](https://gcc.gnu.org/backends.html) )
## Design and Rationale

This is designed to manipulate several compilation-related binaries, related files, and their dependencies, as well as provide an exemplary environment to run them.  Therefore, the container image necessarily looks like a full-fat rpm-based distro instead of a lean function-feature-only container.

The design derives directly from two related AutoSD projects:
- [autosd-buildbox](https://gitlab.com/CentOS/automotive/container-images/autosd-buildbox), which creates a basic image able to build rpms
- [autosd-buildbox-x86_64-to-aarch64-cross-tools](https://gitlab.com/CentOS/automotive/container-images/autosd-buildbox-x86_64-to-aarch64-cross-tools), which adds cross-compilers to above

This project diverges a bit from them because of an anticipated divergence of derivation and features, as well as desire to fixate on image derivation.

## Quick-And-Dirty: I just want to pull down the container and use it

### Container Runtime Dependencies
- Be on a system able to run  a somewhat recent podman or toolbox (podman-toolbox).  The most comprehensive capability for this container is available when using it via Podman 5.0+.  At a minimum, due to the dates of several different dependent features and bug fixes, the simplest thing to say about dependency is "Run this container on a container host with a podman version similar to a recent version of CentOS 9 Stream (ca 202204 or newer), or Ubuntu newer than 21.10.  Otherwise there may be additional work to get this running properly." 

### Run the Already-Built Container from the Registry
There is a pre-built container image.  Any Quay.io links are based on the automatic container build/host service we use where the built container image is published.  To run it, do:
- Be a normal user
- via podman (currently preferred): 
  - this is reflected in the structure of the `make run` command
  - (run command(s) pulling from quay.io url go here)
- via toolbox (advanced users/features): 
  - (FUTURE) this is reflected in the structure of the `make run-toolbox` command
  - (run command(s) pulling from quay.io url go here)

## How to build the Container Yourself

We use the `make` utility to build the container.  This means you can build it locally from scratch.

### Container Build dependencies 
- make
- buildah
- podman
- (optional) toolbox (aka Toolbx)

### Using the container build make helper

The following are the `make` targets that can be used:

```
$ make

Usage:
  make <target>

Help-related tasks
  help                                 Help

Build-related tasks
  build                                Build the container locally (all arches) and print installed test
  manifest                             creates the buildah manifest for multi-arch images
  podman-build                         Build amd64
  podman-build-amd64                   build the container in amd64
  test-amd64                           Prints the test of most tools inside the container amd64
  test                                 Tests the container for all the required bits amd64

Run and upload tasks
  run-podman                           Runs the container interactively
  pack-amd64                           pack up the cross-tools and put it in a shared place
  upload                               Uploads the container to ${UPLOADREGISTRY}/${CONTAINER}
  clean                                Removes any previously built artifact                               Removes any previously built artifact
```

### Build Sequence

There are a few variables that can be set in your shell to build the container:

| VARIABLE    | Description | Default Value |
| -------- | ------- | ------- |
| NAME  | The name of the container.    | autosd-buildbox-x86_64-to-aarch64-cross-tools |
| TAG   | The tag for the container in the Quay repo | latest |
| CONTAINER | Name/Tag name | `$(NAME):$(TAG)` |
| REGISTRY  | Host for local registry | localhost |
| UPLOADREGISTRY | Target registry to upload container image | quay.io/centos-sig-automotive |

To build the container just execute the following:

```
$ make build 
buildah manifest create "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest"
ff13c21cd5011b60527bb111859a830e770f46ec93309360268591735620f760
Building the utility container amd64
buildah bud --arch=amd64 --build-arg TARGETARCH=amd64 --build-arg ALTTARGETARCH=x86_64 \
	--build-arg OPTTARGETARCH='' --build-arg EXTRARPMS='' --format docker \
	-f Containerfile -t "autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64"
STEP 1/7: FROM quay.io/centos-sig-automotive/autosd-buildbox:latest
STEP 2/7: LABEL com.github.containers.toolbox="true"       com.redhat.component="$NAME"       name="$NAME"       version="$VERSION"       usage="This image can be used with the toolbox command"       summary="Base image for building AutoSD packages with cross compilation tools"
...
Complete!
68 files removed
COMMIT autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64
Getting image source signatures
Copying blob 87a1067ceb62 skipped: already exists  
Copying blob b2c18eba08ec skipped: already exists  
Copying blob 53dcb01520cb done   | 
Copying config ee34c3f8c9 done   | 
Writing manifest to image destination
--> ee34c3f8c98c
[Warning] one or more build args were not consumed: [ALTTARGETARCH EXTRARPMS OPTTARGETARCH]
Successfully tagged localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64
ee34c3f8c98c43916b3563242b315a4b4338730f0578ef8199de2c1827eed004
buildah manifest add --arch=amd64 "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest" "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest-amd64"
84bcdc27436a8be7d665e46bc03d93a719e9eea18b6810f05596134f9b758575: sha256:ffcddcd2d2406b7b9b2f3f52d9b86b36a6c16c8bfec467dc76d054eb3faaa479
** WIP: Testing linux/amd64
* Arm64 GCC: 
aarch64-redhat-linux-gcc (GCC) 11.4.1 20231218 (Red Hat 11.4.1-3 cross from x86_64)
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

Once the container builds, you can upload to the target registry defined by UPLOADREGISTRY variable.

```
$ make upload
Uploading the localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest container to quay.io/claudiol/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest
buildah manifest push --all "localhost/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest" "docker://quay.io/claudiol/autosd-buildbox-x86_64-to-aarch64-cross-tools:latest"
Getting image list signatures
Copying 1 images generated from 1 images in list
Copying image sha256:ffcddcd2d2406b7b9b2f3f52d9b86b36a6c16c8bfec467dc76d054eb3faaa479 (1/1)
Getting image source signatures
Copying blob 53dcb01520cb [================================>-----] 2.2GiCopying blob 53dcb01520cb done   | 
Copying blob 36850128dbaa skipped: already exists  
Copying blob 9e92692856f1 skipped: already exists  
Copying config ee34c3f8c9 done   | 
Writing manifest to image destination
Writing manifest list to image destination
Storing list signatures
```

## License

[GPLv2](./LICENSE)
