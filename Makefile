NAME ?= crossland
TAG ?= latest
CONTAINER ?= $(NAME):$(TAG)

CROSSTOOLSEXPORTDIR := /opt/cross-tools

REGISTRY ?= localhost
#UPLOADREGISTRY ?= quay.io/centos-sig-automotive
UPLOADREGISTRY ?= quay.io/rhn-gps-bbenson
TESTCOMMAND := "set -e; echo '* Arm64 GCC: '; aarch64-redhat-linux-gcc --version; "
#TESTCOMMAND := "set -e; echo '* Arm64 GCC: '; aarch64-redhat-linux-gcc -dumpmachine; "
#TESTCOMMAND := "set -e; echo '* Arm64 GCC: '; aarch64-redhat-linux-gcc -xc -E -v; "

PACKCROSSTOOLSCOMMAND := "set -e; cd ${CROSSTOOLSEXPORTDIR} ; tar -czvf /var/tmp/cross-tools.gz *; mv /var/tmp/cross-tools.gz $$(pwd)/; "

##@ Information-related tasks

.PHONY: help
help: ## Help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^(\s|[a-zA-Z_0-9-])+:.*?##/ { printf "  \033[36m%-35s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Build-related tasks

.PHONY: all
all: clean manifest build test pack ## Remove old local container image (!!), build the container locally (default arch/es), run brief test, pack some archives to PWD for external toolchain use, exit

.PHONY: manifest
manifest: ## creates the buildah manifest for multi-arch images
	buildah manifest create "${REGISTRY}/${CONTAINER}"

.PHONY: build
build: build-podman ## Build the container locally (default arch/es)

.PHONY: build-podman
build-podman: build-podman-amd64 ## Build amd64...TODO this is a bit hard-wired

.PHONY: build-podman-amd64
build-podman-amd64: ## build the container in amd64
	@echo "Building the utility container amd64"
	buildah bud --arch=amd64 --build-arg TARGETARCH=amd64 --build-arg ALTTARGETARCH=x86_64 \
		--build-arg OPTTARGETARCH='' --build-arg EXTRARPMS='' --format docker \
		--force-rm=true --no-cache \
		-f Containerfile -t "${CONTAINER}-amd64"
	buildah manifest add --arch=amd64 "${REGISTRY}/${CONTAINER}" "${REGISTRY}/${CONTAINER}-amd64"

##@ Runtime-related tasks

.PHONY: test
test: test-amd64 ## Test the container for basic function and exit (default arch/es), indirectly checks host support

.PHONY: test-amd64
test-amd64: ## Prints a small test from inside the container -amd64
	@echo "** WIP: Testing linux/amd64"
	@podman run --arch=amd64 --rm -it --net=host "${REGISTRY}/${CONTAINER}-amd64" bash -c \
		$(TESTCOMMAND)

.PHONY: run
run: run-podman ## Select a default run mechanism

.PHONY: run-podman
run-podman: ## Runs the container interactively
	podman run --rm -it --net=host \
		--security-opt label=disable \
		-v ${HOME}:/pattern \
		-v ${HOME}:${HOME} \
		-w $$(pwd) "${REGISTRY}/${CONTAINER}-amd64" sh

##@ Maintenance-related tasks

.PHONY: pack
pack: pack-amd64 ##select a default run for pack

.PHONY: pack-amd64
pack-amd64: ## pack up the cross-tools and put it in a shared place
	@echo "** pack the cross-tools via /var/tmp"
	@podman run --arch=amd64 --rm -it \
	--net=host \
	--security-opt label=disable \
	-v ${PWD}:${PWD} \
	-w $$(pwd) "${REGISTRY}/${CONTAINER}-amd64" bash -c \
	$(PACKCROSSTOOLSCOMMAND)

.PHONY: manifest-check
manifest-check: ## checks the buildah manifest for multi-arch images
	buildah manifest TODO "${REGISTRY}/${CONTAINER}"

.PHONY: upload
upload: ## Uploads the container to ${UPLOADREGISTRY}/${CONTAINER}
	@echo "Uploading the ${REGISTRY}/${CONTAINER} container to ${UPLOADREGISTRY}/${CONTAINER}"
	buildah manifest push --all "${REGISTRY}/${CONTAINER}" "docker://${UPLOADREGISTRY}/${CONTAINER}"

.PHONY: clean
clean: ## Removes any previously built artifact
	[[ -f cross-tools.gz ]] && rm cross-tools.gz || echo "no previous cross-tools.gz, continuing..."
	#buildah manifest rm "${REGISTRY}/${CONTAINER}"
	podman image exists "${REGISTRY}/${CONTAINER}" && (echo "image exists"; podman rmi "${REGISTRY}/${CONTAINER}") || echo "image not there, continuing..." 
